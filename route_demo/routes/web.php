<?php
Route::get('/', function () {
    return view('welcome');
});
Route::get('/home','helloController@index');

Route::get('form',function(){
    echo '<form action="form" method="post">';
    echo  '<input type="hidden" name="_token" value="'.csrf_token().'">';

     echo '<select name="_method">';
     echo '<option value="POST">POST</option>';
     echo '<option value="PUT">PUT</option>';
     echo '<option value="DELETE">DELETE</option>';
     echo '</select>';

     echo   '<input type="submit">';
     echo '</form>';
});
Route::post('form',function(){
        return "this is a post method";
});
Route::put('form',function(){
    return "this is a put method";
});
Route::delete('form',function(){
    return "this is a delete method
    ";
});
Route::any('all',function(){
    return "This is a default method";
});
//Routing Paramenter

Route::get('demo/{name}/{age?}',function($name,$age=null){
    return "I AM DEMO".$name.$age;
});
